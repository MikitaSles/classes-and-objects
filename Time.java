public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        setTime(hour, minute, second);
    }

    public void setTime(int hour, int minute, int second) {
        if (hour < 0 || hour > 23) {
            this.hour = 0;
        } else {
            this.hour = hour;
        }
        if (minute < 0 || minute > 59) {
            this.minute = 0;
        } else {
            this.minute = minute;
        }
        if (second < 0 || second > 59) {
            this.second = 0;
        } else {
            this.second = second;
        }
    }

    public void setHour(int hour) {
        if (hour < 0 || hour > 23) {
            this.hour = 0;
        } else {
            this.hour = hour;
        }
    }

    public void setMinute(int minute) {
        if (minute < 0 || minute > 59) {
            this.minute = 0;
        } else {
            this.minute = minute;
        }
    }

    public void setSecond(int second) {
        if (second < 0 || second > 59) {
            this.second = 0;
        } else {
            this.second = second;
        }
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public void addHours(int hours) {
        int newHour = (hour + hours) % 24;
        setHour(newHour);
    }

    public void addMinutes(int minutes) {
        int totalMinutes = hour * 60 + minute + minutes;
        int newHour = totalMinutes / 60 % 24;
        int newMinute = totalMinutes % 60;
        setHour(newHour);
        setMinute(newMinute);
    }

    public void addSeconds(int seconds) {
        int totalSeconds = hour * 3600 + minute * 60 + second + seconds;
        int newHour = totalSeconds / 3600 % 24;
        int newMinute = totalSeconds / 60 % 60;
        int newSecond = totalSeconds % 60;
        setHour(newHour);
        setMinute(newMinute);
        setSecond(newSecond);
    }

    public String toString() {
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }
}

