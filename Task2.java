public class Task2 {
    private int variable1;
    private String variable2;

    // Конструктор с параметрами
    public Task2(int variable1, String variable2) {
        this.variable1 = variable1;
        this.variable2 = variable2;
    }

    // Конструктор по умолчанию
    public Task2() {
        this.variable1 = 0;
        this.variable2 = "";
    }

    // Методы доступа к переменным
    public int getVariable1() {
        return variable1;
    }

    public void setVariable1(int variable1) {
        this.variable1 = variable1;
    }

    public String getVariable2() {
        return variable2;
    }

    public void setVariable2(String variable2) {
        this.variable2 = variable2;
    }
}

