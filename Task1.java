public class Task1 {
    private int variable1;
    private int variable2;

    public Task1(int variable1, int variable2) {
        this.variable1 = variable1;
        this.variable2 = variable2;
    }

    public void displayVariables() {
        System.out.println("Variable 1: " + variable1);
        System.out.println("Variable 2: " + variable2);
    }

    public void setVariable1(int value) {
        variable1 = value;
    }

    public void setVariable2(int value) {
        variable2 = value;
    }

    public int getSum() {
        return variable1 + variable2;
    }

    public int getMax() {
        return Math.max(variable1, variable2);
    }
}

