public class Main {
    public static void main(String[] args) {
        // Создание экземпляра класса Task1
        Task1 task = new Task1(10, 20);

        // Вывод значений переменных
        task.displayVariables(); // Variable 1: 10, Variable 2: 20

        // Получение суммы значений переменных
        int sum = task.getSum();
        System.out.println("Sum of variables: " + sum); // Sum of variables: 30

        // Получение максимального значения переменных
        int max = task.getMax();
        System.out.println("Maximum of variables: " + max); // Maximum of variables: 20

        // Изменение значений переменных
        task.setVariable1(15);
        task.setVariable2(25);

        // Вывод обновленных значений переменных
        task.displayVariables(); // Variable 1: 15, Variable 2: 25
    }
}
