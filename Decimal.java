public class Decimal{
    private int minValue;
    private int maxValue;
    private int currentValue;

    public Decimal() {
        this(0, 10, 0);
    }

    public Decimal(int minValue, int maxValue, int currentValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.currentValue = currentValue;
    }

    public void increment() {
        if (currentValue < maxValue) {
            currentValue++;
        }
    }

    public void decrement() {
        if (currentValue > minValue) {
            currentValue--;
        }
    }

    public int getCurrentValue() {
        return currentValue;
    }
}
